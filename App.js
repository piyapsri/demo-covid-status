import React, { Component } from 'react'
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Platform
} from 'react-native';

const statusbar = Platform.OS == 'ios' ? 40 : 0
const now = new Date();
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      response: {},
      lastUpdate: now.getDate() + '/' + now.getMonth() + '/' + now.getFullYear()
    }
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    fetch('https://covid19.th-stat.com/api/open/today').then((response) => {
      return response.json();
    })
      .then((data) => {
        console.log(data);
        this.setState({ response: data })
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <ScrollView
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionTitle}>
              <Text style={styles.titleText}>Thailand Covid Status</Text>
              <Text style={styles.titleTime}>
                Last update <Text style={styles.highlight}>{this.state.response.UpdateDate}</Text>
              </Text>
            </View>
            <View style={styles.sectionContent}>
              <Text style={styles.titleText}>ยอดสะสม</Text>
              <Text style={styles.desc}>{this.state.response.Confirmed}</Text>
            </View>
            <View style={styles.sectionContent}>
              <Text style={styles.titleText}>รักษาหาย</Text>
              <Text style={styles.desc}>{this.state.response.Recovered}</Text>
            </View>
            <View style={styles.sectionContent}>
              <Text style={styles.titleText}>รักษาอยู่</Text>
              <Text style={styles.desc}>{this.state.response.Hospitalized}</Text>
            </View>
            <View style={styles.sectionContent}>
              <Text style={styles.titleText}>เสียชีวิต</Text>
              <Text style={styles.desc}>{this.state.response.Deaths}</Text>
            </View>
            <View style={styles.sectionContent}>
              <Text style={styles.titleText}>ผู้ปวยใหม่</Text>
              <Text style={styles.desc}>{this.state.response.NewConfirmed}</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: statusbar
  },
  scrollView: {
    backgroundColor: "#eee"
  },
  sectionTitle: {
    paddingHorizontal: 15,
    textAlign: 'center',
    paddingVertical: '10%',
    backgroundColor: '#eee'
  },
  sectionContent: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderBottomColor: '#eee',
    borderBottomWidth: 1
  },
  titleText: {
    fontSize: 28,
    color: '#333',
  },
  titleTime: {
    color: '#aaa',
    fontSize: 18,
    marginTop: 10
  },
  desc: {
    color: 'orange',
    fontSize: 18,
    marginTop: 10
  },
  body: {
    backgroundColor: "#ffffff",
  }
})
